def echo(what_to_say)
  what_to_say
end

def shout(what_to_say)
  what_to_say.upcase
end

def repeat(what_to_say, how_many_times = 2)
  simon_says = what_to_say
  (1...how_many_times).each { simon_says += " #{what_to_say}" }
  simon_says
end

def start_of_word(what_to_say, how_many_letters = 1)
  what_to_say[0...how_many_letters]
end

def first_word(what_to_say)
  what_to_say.split[0]
end

def titleize(what_to_say)
  do_not_capital=['the', 'over', 'and']
  what_to_say.split.map.with_index do |word, i|
    if i.zero?
      word[0].capitalize + word[1..-1]
    elsif !do_not_capital.member?(word)
      word[0].capitalize + word[1..-1]
    else
      word
    end
  end.join(' ')
end
