def add(first_num, second_num)
  first_num + second_num
end

def subtract(first_num, second_num)
  first_num - second_num
end

def sum(numbers)
  numbers.inject(0) {|total, number| total + number}
end
