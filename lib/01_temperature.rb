def ftoc(temperature)
  (temperature - 32.0) / 9.0 * 5.0
end

def ctof(temperature)
  temperature / 5.0 * 9.0 + 32.0
end
