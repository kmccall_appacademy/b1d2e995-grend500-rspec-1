require 'byebug'

def translate(sentence)
  sentence.split.map { |word| translateword(word)}.join(' ')
end

def translateword(word)
  vowels = %w[a e i o u]
  if vowels.member?(word[0])
    word + 'ay'
  elsif word[0] == 'q'
    translate(word[2..-1] + word[0..1])
  else
    translate(word[1..-1] + word[0])
  end
end
